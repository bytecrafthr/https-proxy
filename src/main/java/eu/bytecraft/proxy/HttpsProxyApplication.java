package eu.bytecraft.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpsProxyApplication {
    public static void main(String[] args) {
        SpringApplication.run(HttpsProxyApplication.class, args);
    }
}
