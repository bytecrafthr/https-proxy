package eu.bytecraft.proxy.client;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.nio.charset.Charset;

@Data
@Slf4j
public class GenericProxyWsClient {

    private HttpClient httpClient;
    private String wsUri;
    private String wsName;
    private String soapActionHeaderName;
    private String CONTENTTYPE_HEADER = "Content-type";
    private String CONTENTTYPE_VALUE = "text/xml;charset=UTF-8";
    private Charset UTF8 = Charset.forName("UTF-8");

    public GenericProxyWsClient(final HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public InternalResponse invoke(final String request, final String soapAction, final String contextPath) {

        log.info("Invoking {} with request\n{}\n", wsName, request);

        HttpPost httpPost = new HttpPost(wsUri + contextPath);
        httpPost.setEntity(new StringEntity(request, UTF8));
        if(StringUtils.isNotBlank(soapAction))
            httpPost.setHeader(soapActionHeaderName, soapAction);

        httpPost.setHeader(CONTENTTYPE_HEADER, CONTENTTYPE_VALUE);
        HttpResponse httpResponse;
        String content;
        InternalResponse response = new InternalResponse();

        try {
            httpResponse = httpClient.execute(httpPost);
            response.setCode(httpResponse.getStatusLine().getStatusCode());
            content = EntityUtils.toString(httpResponse.getEntity(), UTF8.name());
            response.setContent(content);
        } catch (final IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }

        log.info("Service {} received response\n{}\n", wsName, content);

        return response;
    }
}

