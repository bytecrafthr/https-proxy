package eu.bytecraft.proxy.client;

import lombok.Data;

@Data
public class InternalResponse {
    private int code;
    private String content;
}
