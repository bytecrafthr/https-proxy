package eu.bytecraft.proxy.api;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import eu.bytecraft.proxy.client.InternalResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@Slf4j
public class ProxyController {

    private Map<String, GenericProxyWsClient> clientMap;

    public ProxyController(
            @Qualifier(value = "enarucivanjeClient") final GenericProxyWsClient enarucivanjeClient,
            @Qualifier(value = "enovorodjenceClient") final GenericProxyWsClient enovorodjenceClient,
            @Qualifier(value = "crvenaUputnicaClient") final GenericProxyWsClient crvenaUputnicaClient,
            @Qualifier(value = "osiginfoClient") final GenericProxyWsClient osiginfoClient)
    {
        clientMap = new HashMap<>();
        clientMap.put("enarucivanje", enarucivanjeClient);
        clientMap.put("enovorodjence", enovorodjenceClient);
        clientMap.put("crvena", crvenaUputnicaClient);
        clientMap.put("osiginfo", osiginfoClient);
    }

    @PostMapping(path = "/proxy/{clientKey}", produces = { "text/xml;charset=UTF-8" })
    @ResponseBody
    public ResponseEntity<String> proxy(
            @PathVariable String clientKey,
            @RequestBody String request,
            @RequestHeader(value = "SOAPAction", required = false) String soapAction,
            @RequestHeader(value = "ContextPath", required = false) String contextPath)
    {
        InternalResponse internalResponse =
                clientMap.get(clientKey).invoke(
                        request,
                        StringUtils.defaultString(soapAction),
                        StringUtils.defaultString(StringUtils.prependIfMissing(contextPath, "/")));

        ResponseEntity<String> responseEntity = ResponseEntity.status(internalResponse.getCode()).body(internalResponse.getContent());
        return responseEntity;
    }
}
