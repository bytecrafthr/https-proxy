package eu.bytecraft.proxy.config;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ENarucivanjeBisConfig extends AbstractWsInstanceConfig {

    @Autowired
    public ENarucivanjeBisConfig(
            @Value("${enarucivanje.ssl-mutual}") boolean sslMutual,
            @Value("${enarucivanje.timeout}") int timeout,
            @Value("${enarucivanje.ssl-alias}") String sslAlias)
    {
        super(sslMutual, timeout, sslAlias);
    }

    @Bean(name = "enarucivanjeHttpClient")
    public HttpClient httpClient() throws Exception {
        return httpClientInternal();
    }

    @Bean(name = "enarucivanjeClient")
    @ConfigurationProperties(prefix = "enarucivanje")
    public GenericProxyWsClient wsClient(
            @Autowired @Qualifier(value = "enarucivanjeHttpClient") HttpClient httpClient)
    {
        return new GenericProxyWsClient(httpClient);
    }
}
