package eu.bytecraft.proxy.config;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ENovorodjenceConfig extends AbstractWsInstanceConfig {

    @Autowired
    public ENovorodjenceConfig(
            @Value("${enovorodjence.ssl-mutual}") boolean sslMutual,
            @Value("${enovorodjence.timeout}") int timeout,
            @Value("${enovorodjence.ssl-alias}") String sslAlias)
    {
        super(sslMutual, timeout, sslAlias);
    }

    @Bean(name = "enovorodjenceHttpClient")
    public HttpClient httpClient() throws Exception {
       return httpClientInternal();
    }

    @Bean(name = "enovorodjenceClient")
    @ConfigurationProperties(prefix = "enovorodjence")
    public GenericProxyWsClient wsClient(
            @Autowired @Qualifier(value = "enovorodjenceHttpClient") HttpClient httpClient)
    {
        return new GenericProxyWsClient(httpClient);
    }

}

