package eu.bytecraft.proxy.config;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OsigInfoConfig extends AbstractWsInstanceConfig {

    @Autowired
    public OsigInfoConfig(
            @Value("${osiginfo.ssl-mutual}") boolean sslMutual,
            @Value("${osiginfo.timeout}") int timeout,
            @Value("${osiginfo.ssl-alias}") String sslAlias)
    {
        super(sslMutual, timeout, sslAlias);
    }

    @Bean(name = "osiginfoHttpClient")
    public HttpClient httpClient() throws Exception {
        return httpClientInternal();
    }

    @Bean(name = "osiginfoClient")
    @ConfigurationProperties(prefix = "osiginfo")
    public GenericProxyWsClient wsClient(
            @Autowired @Qualifier(value = "osiginfoHttpClient") HttpClient httpClient)
    {
        return new GenericProxyWsClient(httpClient);
    }
}
