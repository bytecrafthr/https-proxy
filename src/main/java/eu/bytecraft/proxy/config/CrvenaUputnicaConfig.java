package eu.bytecraft.proxy.config;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CrvenaUputnicaConfig extends AbstractWsInstanceConfig {

    @Autowired
    public CrvenaUputnicaConfig(
            @Value("${crvena.ssl-mutual}") boolean sslMutual,
            @Value("${crvena.timeout}") int timeout,
            @Value("${crvena.ssl-alias}") String sslAlias)
    {
        super(sslMutual, timeout, sslAlias);
    }

    @Bean(name = "crvenaUputnicaHttpClient")
    public HttpClient httpClient() throws Exception {
        return httpClientInternal();
    }

    @Bean(name = "crvenaUputnicaClient")
    @ConfigurationProperties(prefix = "crvena")
    public GenericProxyWsClient wsClient(
            @Autowired @Qualifier(value = "crvenaUputnicaHttpClient") HttpClient httpClient)
    {
        return new GenericProxyWsClient(httpClient);
    }
}
