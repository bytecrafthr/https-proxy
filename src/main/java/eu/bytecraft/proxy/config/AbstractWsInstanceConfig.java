package eu.bytecraft.proxy.config;

import eu.bytecraft.proxy.client.GenericProxyWsClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import javax.net.ssl.SSLContext;
import java.net.Socket;
import java.util.Map;

abstract class AbstractWsInstanceConfig {

    private boolean sslMutual;
    private int timeout;
    private String sslAlias;

    @Value("${client.max.http.connections}")
    private int maxHttpConnections;

    @Value("${client.ssl.trust-store}")
    private Resource trustStore;

    @Value("${client.ssl.trust-store-password}")
    private String trustStorePassword;

    @Value("${client.ssl.key-store}")
    private Resource keyStore;

    @Value("${client.ssl.key-store-password}")
    private String keyStorePassword;

    @Value("${client.ssl.key-password}")
    private String keyPassword;

    AbstractWsInstanceConfig(final boolean sslMutual, final int timeout, final String sslAlias) {
        this.sslMutual = sslMutual;
        this.timeout = timeout;
        this.sslAlias = sslAlias;
    }

    HttpClient httpClientInternal() throws Exception {
        return this.sslMutual ? httpSslClient(sslConnectionSocketFactory()) : httpNoSslClient();
    }

    private HttpClient httpNoSslClient() {
        return HttpClientBuilder.create().setMaxConnTotal(maxHttpConnections)
                .addInterceptorFirst(new HttpComponentsMessageSender.RemoveSoapHeadersInterceptor())
                .setDefaultRequestConfig(requestConfig()).build();
    }

    private HttpClient httpSslClient(final SSLConnectionSocketFactory sslConnectionSocketFactory) {
        return HttpClientBuilder.create().setMaxConnTotal(maxHttpConnections)
                .setSSLSocketFactory(sslConnectionSocketFactory)
                .addInterceptorFirst(new HttpComponentsMessageSender.RemoveSoapHeadersInterceptor())
                .setDefaultRequestConfig(requestConfig()).build();
    }

    private RequestConfig requestConfig() {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        return config;
    }

    private SSLConnectionSocketFactory sslConnectionSocketFactory() throws Exception {
        // NoopHostnameVerifier essentially turns hostname verification off as otherwise following error
        // is thrown: java.security.cert.CertificateException: No name matching localhost found
        return new SSLConnectionSocketFactory(sslContext(), NoopHostnameVerifier.INSTANCE);
    }

    private SSLContext sslContext() throws Exception {
        return SSLContextBuilder.create()
                .loadKeyMaterial(keyStore.getURL(), keyStorePassword.toCharArray(), keyPassword.toCharArray(), keyStrategy())
                .loadTrustMaterial(trustStore.getURL(), trustStorePassword.toCharArray()).build();
    }

    private PrivateKeyStrategy keyStrategy() {
        return (aliases, socket) -> {
            for (String alias : aliases.keySet()) {
                if (alias.contains(sslAlias.trim())) {
                    return alias;
                }
            }
            return null;
        };
    }

    abstract HttpClient httpClient() throws Exception;

    abstract GenericProxyWsClient wsClient(HttpClient httpClient);
}
