import io.gatling.core.Predef._

class BasicSimulation1 extends AbstractSimulation { // 3

  val scn = buildScenario(
    "/zuta",
    """<FahrenheitToCelsius xmlns="https://www.w3schools.com/xml/"><Fahrenheit>122</Fahrenheit></FahrenheitToCelsius>""",
    "Zuta");

  setUp ( // 11
    scn.inject(atOnceUsers(30)) // 12
  ).protocols(httpProtocol) // 13

}