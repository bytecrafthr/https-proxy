import io.gatling.core.Predef._


class BasicSimulation extends AbstractSimulation { // 3

  val scn = buildScenario(
    "/crvena",
    """<CelsiusToFahrenheit xmlns="https://www.w3schools.com/xml/"><Celsius>50</Celsius></CelsiusToFahrenheit>""",
    "Crvena");

  setUp ( // 11
    scn.inject(atOnceUsers(30)) // 12
  ).protocols(httpProtocol) // 13

}