import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

abstract class AbstractSimulation extends Simulation {

  val httpProtocol = http // 4
    .baseUrl("http://localhost:9099") // 5
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  def buildScenario(url: String, body: String, scenarioName: String) : ScenarioBuilder = {
    return scenario(scenarioName)
      .exec(http("request_"+scenarioName)
      .post(url)
      .body(StringBody(body)))
      .pause(5); // 10
  }


}
